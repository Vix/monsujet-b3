Travail Collaboratif (Git, etc.) (22/09/2020)
Initiation au travail collaboratif  :

prise de notes collaborative avec FramaPad ;
création et utilisation d'un compte FramaGit (GitLab) ;
présentation de l'outil git ;
test des commandes push et pull.